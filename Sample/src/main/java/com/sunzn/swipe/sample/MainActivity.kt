package com.sunzn.swipe.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sunzn.swipe.library.OnRefreshListener
import com.sunzn.swipe.library.SwipeToLoadLayout

class MainActivity : AppCompatActivity(), OnRefreshListener {

    private var swipeToLoadLayout: SwipeToLoadLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        swipeToLoadLayout = findViewById(R.id.swipeToLoadLayout)
        swipeToLoadLayout?.setOnRefreshListener(this)
        swipeToLoadLayout?.postDelayed({ swipeToLoadLayout?.isRefreshing = true }, 100)
    }

    override fun onPrepare() {}
    override fun onRefresh() {
        swipeToLoadLayout?.postDelayed({ swipeToLoadLayout!!.isRefreshing = false }, 3000)
    }

    override fun onReset() {}
    override fun onCancel() {}
}