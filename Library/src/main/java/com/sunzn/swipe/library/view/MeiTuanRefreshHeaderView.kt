package com.sunzn.swipe.library.view

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class MeiTuanRefreshHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context, attrs, defStyleAttr) {

    private lateinit var icon: AppCompatImageView
    private lateinit var main: LinearLayoutCompat
    private lateinit var mAnimDrawable: AnimationDrawable
    private val mHeaderHeight: Int

    init {
        mHeaderHeight = context.resources.getDimensionPixelOffset(R.dimen.refresh_header_height_100)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        icon = findViewById(R.id.swipe_meituan_icon)
        main = findViewById(R.id.swipe_meituan_main)
        mAnimDrawable = icon.background as AnimationDrawable
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onRefresh() {
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onPrepare() {
        main.alpha = 0.3f
        icon.scaleX = 0.4f
        icon.scaleY = 0.4f
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            val scale = y.toFloat() / mHeaderHeight.toFloat()
            if (y >= mHeaderHeight) {
                icon.scaleX = 1f
                icon.scaleY = 1f
                main.alpha = 1.0f
            } else if (y > 0) {
                icon.scaleX = scale
                icon.scaleY = scale
                main.alpha = scale
            } else {
                icon.scaleX = 0.4f
                icon.scaleY = 0.4f
                main.alpha = 0.3f
            }
        }
    }

    override fun onRelease() {
        mAnimDrawable.stop()
    }

    override fun onComplete() {}
    override fun onReset() {
        mAnimDrawable.stop()
        main.alpha = 1.0f
    }

    override fun onCancel() {}

}