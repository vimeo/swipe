package com.sunzn.swipe.library

/**
 * Created by sunzn on 2016/5/30.
 */
interface SwipeTrigger {
    fun onPrepare()
    fun onMove(y: Int, isComplete: Boolean, automatic: Boolean)
    fun onRelease()
    fun onComplete()
    fun onReset()
    fun onCancel()
}