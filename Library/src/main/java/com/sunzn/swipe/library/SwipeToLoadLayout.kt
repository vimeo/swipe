package com.sunzn.swipe.library

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.widget.Scroller
import com.sunzn.swipe.library.SwipeToLoadLayout
import kotlin.math.abs

/**
 * Created by sunzn on 2016/5/30.
 */
class SwipeToLoadLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ViewGroup(context, attrs, defStyleAttr) {

    companion object {
        private val TAG = SwipeToLoadLayout::class.java.simpleName
        private const val DEFAULT_SWIPING_TO_REFRESH_TO_DEFAULT_SCROLLING_DURATION = 200
        private const val DEFAULT_RELEASE_TO_REFRESHING_SCROLLING_DURATION = 200
        private const val DEFAULT_REFRESH_COMPLETE_DELAY_DURATION = 300
        private const val DEFAULT_REFRESH_COMPLETE_TO_DEFAULT_SCROLLING_DURATION = 1000
        private const val DEFAULT_DEFAULT_TO_REFRESHING_SCROLLING_DURATION = 500
        private const val DEFAULT_SWIPING_TO_LOAD_MORE_TO_DEFAULT_SCROLLING_DURATION = 200
        private const val DEFAULT_RELEASE_TO_LOADING_MORE_SCROLLING_DURATION = 200
        private const val DEFAULT_LOAD_MORE_COMPLETE_DELAY_DURATION = 300
        private const val DEFAULT_LOAD_MORE_COMPLETE_TO_DEFAULT_SCROLLING_DURATION = 300
        private const val DEFAULT_DEFAULT_TO_LOADING_MORE_SCROLLING_DURATION = 300

        /**
         * how hard to drag
         */
        private const val DEFAULT_DRAG_RATIO = 0.5f
        private const val INVALID_POINTER = -1
        private const val INVALID_COORDINATE = -1
    }

    private val mAutoScroller: AutoScroller
    private var mRefreshListener: OnRefreshListener? = null
    private var mLoadMoreListener: OnLoadMoreListener? = null
    private var mHeaderView: View? = null
    private var mTargetView: View? = null
    private var mFooterView: View? = null
    private var mHeaderHeight = 0
    private var mFooterHeight = 0
    private var mHasHeaderView = false
    private var mHasFooterView = false

    /**
     * indicate whether in debug mode
     */
    private var mDebug = false
    private var mDragRatio = DEFAULT_DRAG_RATIO
    private var mAutoLoading = false

    /**
     * the threshold of the touch event
     */
    private val mTouchSlop: Int

    /**
     * status of SwipeToLoadLayout
     */
    private var mStatus = STATUS.STATUS_DEFAULT

    /**
     * target view top offset
     */
    private var mHeaderOffset = 0

    /**
     * target offset
     */
    private var mTargetOffset = 0

    /**
     * target view bottom offset
     */
    private var mFooterOffset = 0

    /**
     * init touch action down point.y
     */
    private var mInitDownY = 0f

    /**
     * init touch action down point.x
     */
    private var mInitDownX = 0f

    /**
     * last touch point.y
     */
    private var mLastY = 0f

    /**
     * last touch point.x
     */
    private var mLastX = 0f

    /**
     * action touch pointer's id
     */
    private var mActivePointerId = 0
    /**
     * is refresh function is enabled
     *
     * @return
     */
    /**
     * switch refresh function on or off
     *
     * @param enable
     */
    /**
     * **ATTRIBUTE:**
     * a switcher indicate whither refresh function is enabled
     */
    var isRefreshEnabled = true
    /**
     * is load more function is enabled
     *
     * @return
     */
    /**
     * switch load more function on or off
     *
     * @param enable
     */
    /**
     * **ATTRIBUTE:**
     * a switcher indicate whiter load more function is enabled
     */
    var isLoadMoreEnabled = true

    /**
     * **ATTRIBUTE:**
     * the style default classic
     */
    private var mStyle = STYLE.CLASSIC

    /**
     * **ATTRIBUTE:**
     * offset to trigger refresh
     */
    private var mRefreshTriggerOffset = 0f

    /**
     * **ATTRIBUTE:**
     * offset to trigger load more
     */
    private var mLoadMoreTriggerOffset = 0f

    /**
     * **ATTRIBUTE:**
     * the max value of top offset
     */
    private var mRefreshFinalDragOffset = 0f

    /**
     * **ATTRIBUTE:**
     * the max value of bottom offset
     */
    private var mLoadMoreFinalDragOffset = 0f

    /**
     * **ATTRIBUTE:**
     * Scrolling duration swiping to refresh -> default
     */
    private var mSwipingToRefreshToDefaultScrollingDuration = DEFAULT_SWIPING_TO_REFRESH_TO_DEFAULT_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status release to refresh -> refreshing
     */
    private var mReleaseToRefreshToRefreshingScrollingDuration = DEFAULT_RELEASE_TO_REFRESHING_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Refresh complete delay duration
     */
    private var mRefreshCompleteDelayDuration = DEFAULT_REFRESH_COMPLETE_DELAY_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status refresh complete -> default
     * [.setRefreshing] false
     */
    private var mRefreshCompleteToDefaultScrollingDuration = DEFAULT_REFRESH_COMPLETE_TO_DEFAULT_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status default -> refreshing, mainly for auto refresh
     * [.setRefreshing] true
     */
    private var mDefaultToRefreshingScrollingDuration = DEFAULT_DEFAULT_TO_REFRESHING_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status release to loading more -> loading more
     */
    private var mReleaseToLoadMoreToLoadingMoreScrollingDuration = DEFAULT_RELEASE_TO_LOADING_MORE_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Load more complete delay duration
     */
    private var mLoadMoreCompleteDelayDuration = DEFAULT_LOAD_MORE_COMPLETE_DELAY_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status load more complete -> default
     * [.setLoadingMore] false
     */
    private var mLoadMoreCompleteToDefaultScrollingDuration = DEFAULT_LOAD_MORE_COMPLETE_TO_DEFAULT_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration swiping to load more -> default
     */
    private var mSwipingToLoadMoreToDefaultScrollingDuration = DEFAULT_SWIPING_TO_LOAD_MORE_TO_DEFAULT_SCROLLING_DURATION

    /**
     * **ATTRIBUTE:**
     * Scrolling duration status default -> loading more, mainly for auto load more
     * [.setLoadingMore] true
     */
    private var mDefaultToLoadingMoreScrollingDuration = DEFAULT_DEFAULT_TO_LOADING_MORE_SCROLLING_DURATION

    /**
     * the style enum
     */
    object STYLE {
        const val CLASSIC = 0
        const val ABOVE = 1
        const val BLEW = 2
        const val SCALE = 3
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val childNum = childCount
        when (childNum) {
            0 -> {
                // no child return
                return
            }

            in 1..3 -> {
                mHeaderView = findViewById(R.id.swipe_refresh_header)
                mTargetView = findViewById(R.id.swipe_target)
                mFooterView = findViewById(R.id.swipe_load_more_footer)
            }

            else -> {
                // more than three children: unsupported!
                throw IllegalStateException("Children num must equal or less than 3")
            }
        }
        if (mTargetView == null) {
            return
        }
        if (mHeaderView != null && mHeaderView is SwipeTrigger) {
            (mHeaderView as View).visibility = GONE
        }
        if (mFooterView != null && mFooterView is SwipeTrigger) {
            (mFooterView as View).visibility = GONE
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // header
        if (mHeaderView != null) {
            val headerView: View = mHeaderView as View
            measureChildWithMargins(headerView, widthMeasureSpec, 0, heightMeasureSpec, 0)
            val lp = headerView.layoutParams as MarginLayoutParams
            mHeaderHeight = headerView.measuredHeight + lp.topMargin + lp.bottomMargin
            if (mRefreshTriggerOffset < mHeaderHeight) {
                mRefreshTriggerOffset = mHeaderHeight.toFloat()
            }
        }
        // target
        if (mTargetView != null) {
            val targetView: View = mTargetView as View
            measureChildWithMargins(targetView, widthMeasureSpec, 0, heightMeasureSpec, 0)
        }
        // footer
        if (mFooterView != null) {
            val footerView: View = mFooterView as View
            measureChildWithMargins(footerView, widthMeasureSpec, 0, heightMeasureSpec, 0)
            val lp = footerView.layoutParams as MarginLayoutParams
            mFooterHeight = footerView.measuredHeight + lp.topMargin + lp.bottomMargin
            if (mLoadMoreTriggerOffset < mFooterHeight) {
                mLoadMoreTriggerOffset = mFooterHeight.toFloat()
            }
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        layoutChildren()
        mHasHeaderView = mHeaderView != null
        mHasFooterView = mFooterView != null
    }

    /**
     * TODO add gravity
     * LayoutParams of RefreshLoadMoreLayout
     */
    class LayoutParams : MarginLayoutParams {
        constructor(c: Context?, attrs: AttributeSet?) : super(c, attrs)
        constructor(width: Int, height: Int) : super(width, height)
        constructor(source: MarginLayoutParams?) : super(source)
        constructor(source: ViewGroup.LayoutParams?) : super(source)
    }

    /**
     * {@inheritDoc}
     */
    override fun generateDefaultLayoutParams(): ViewGroup.LayoutParams {
        return LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    /**
     * {@inheritDoc}
     */
    override fun generateLayoutParams(p: ViewGroup.LayoutParams): ViewGroup.LayoutParams {
        return LayoutParams(p)
    }

    /**
     * {@inheritDoc}
     */
    override fun generateLayoutParams(attrs: AttributeSet): ViewGroup.LayoutParams {
        return LayoutParams(context, attrs)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP ->                 // swipeToRefresh -> finger up -> finger down if the status is still swipeToRefresh
                // in onInterceptTouchEvent ACTION_DOWN event will stop the scroller
                // if the event pass to the child view while ACTION_MOVE(condition is false)
                // in onInterceptTouchEvent ACTION_MOVE the ACTION_UP or ACTION_CANCEL will not be
                // passed to onInterceptTouchEvent and onTouchEvent. Instead It will be passed to
                // child view's onTouchEvent. So we must deal this situation in dispatchTouchEvent
                onActivePointerUp()
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mActivePointerId = event.getPointerId(0)
                run {
                    mLastY = getMotionEventY(event, mActivePointerId)
                    mInitDownY = mLastY
                }
                run {
                    mLastX = getMotionEventX(event, mActivePointerId)
                    mInitDownX = mLastX
                }

                // if it isn't an ing status or default status
                if (STATUS.isSwipingToRefresh(mStatus) || STATUS.isSwipingToLoadMore(mStatus) ||
                    STATUS.isReleaseToRefresh(mStatus) || STATUS.isReleaseToLoadMore(mStatus)
                ) {
                    // abort autoScrolling, not trigger the method #autoScrollFinished()
                    mAutoScroller.abortIfRunning()
                    if (mDebug) {
                        Log.i(TAG, "Another finger down, abort auto scrolling, let the new finger handle")
                    }
                }
                if (STATUS.isSwipingToRefresh(mStatus) || STATUS.isReleaseToRefresh(mStatus)
                    || STATUS.isSwipingToLoadMore(mStatus) || STATUS.isReleaseToLoadMore(mStatus)
                ) {
                    return true
                }
            }

            MotionEvent.ACTION_MOVE -> {
                if (mActivePointerId == INVALID_POINTER) {
                    return false
                }
                val y = getMotionEventY(event, mActivePointerId)
                val x = getMotionEventX(event, mActivePointerId)
                val yInitDiff = y - mInitDownY
                val xInitDiff = x - mInitDownX
                mLastY = y
                mLastX = x
                val moved = Math.abs(yInitDiff) > Math.abs(xInitDiff)
                val triggerCondition =  // refresh trigger condition
                    yInitDiff > 0 && moved && onCheckCanRefresh() || yInitDiff < 0 && moved && onCheckCanLoadMore()
                if (triggerCondition) {
                    // if the refresh's or load more's trigger condition  is true,
                    // intercept the move action event and pass it to SwipeToLoadLayout#onTouchEvent()
                    return true
                }
            }

            MotionEvent.ACTION_POINTER_UP -> {
                onSecondaryPointerUp(event)
                mLastY = getMotionEventY(event, mActivePointerId)
                mInitDownY = mLastY
                mLastX = getMotionEventX(event, mActivePointerId)
                mInitDownX = mLastX
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> mActivePointerId = INVALID_POINTER
        }
        return super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                mActivePointerId = event.getPointerId(0)
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                // take over the ACTION_MOVE event from SwipeToLoadLayout#onInterceptTouchEvent()
                // if condition is true
                val y = getMotionEventY(event, mActivePointerId)
                val x = getMotionEventX(event, mActivePointerId)
                val yDiff = y - mLastY
                val xDiff = x - mLastX
                mLastY = y
                mLastX = x
                if (abs(xDiff) > abs(yDiff) && abs(xDiff) > mTouchSlop) {
                    return false
                }
                when {
                    STATUS.isStatusDefault(mStatus) -> {
                        if (yDiff > 0 && onCheckCanRefresh()) {
                            mRefreshCallback.onPrepare()
                            setStatus(STATUS.STATUS_SWIPING_TO_REFRESH)
                        } else if (yDiff < 0 && onCheckCanLoadMore()) {
                            mLoadMoreCallback.onPrepare()
                            setStatus(STATUS.STATUS_SWIPING_TO_LOAD_MORE)
                        }
                    }

                    STATUS.isRefreshStatus(mStatus) -> {
                        if (mTargetOffset <= 0) {
                            setStatus(STATUS.STATUS_DEFAULT)
                            fixCurrentStatusLayout()
                            return false
                        }
                    }

                    STATUS.isLoadMoreStatus(mStatus) -> {
                        if (mTargetOffset >= 0) {
                            setStatus(STATUS.STATUS_DEFAULT)
                            fixCurrentStatusLayout()
                            return false
                        }
                    }
                }
                when {
                    STATUS.isRefreshStatus(mStatus) -> {
                        if (STATUS.isSwipingToRefresh(mStatus) || STATUS.isReleaseToRefresh(mStatus)) {
                            if (mTargetOffset >= mRefreshTriggerOffset) {
                                setStatus(STATUS.STATUS_RELEASE_TO_REFRESH)
                            } else {
                                setStatus(STATUS.STATUS_SWIPING_TO_REFRESH)
                            }
                            fingerScroll(yDiff)
                        }
                    }

                    STATUS.isLoadMoreStatus(mStatus) -> {
                        if (STATUS.isSwipingToLoadMore(mStatus) || STATUS.isReleaseToLoadMore(mStatus)) {
                            if (-mTargetOffset >= mLoadMoreTriggerOffset) {
                                setStatus(STATUS.STATUS_RELEASE_TO_LOAD_MORE)
                            } else {
                                setStatus(STATUS.STATUS_SWIPING_TO_LOAD_MORE)
                            }
                            fingerScroll(yDiff)
                        }
                    }
                }
                return true
            }

            MotionEvent.ACTION_POINTER_DOWN -> {
                val pointerIndex = event.actionIndex
                val pointerId = event.getPointerId(pointerIndex)
                if (pointerId != INVALID_POINTER) {
                    mActivePointerId = pointerId
                }
                mLastY = getMotionEventY(event, mActivePointerId)
                mInitDownY = mLastY
                mLastX = getMotionEventX(event, mActivePointerId)
                mInitDownX = mLastX
            }

            MotionEvent.ACTION_POINTER_UP -> {
                onSecondaryPointerUp(event)
                mLastY = getMotionEventY(event, mActivePointerId)
                mInitDownY = mLastY
                mLastX = getMotionEventX(event, mActivePointerId)
                mInitDownX = mLastX
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                mRefreshCallback.onCancel()
                if (mActivePointerId == INVALID_POINTER) {
                    return false
                }
                mActivePointerId = INVALID_POINTER
            }

            else -> {}
        }
        return super.onTouchEvent(event)
    }

    /**
     * set debug mode(default value false)
     *
     * @param debug if true log on, false log off
     */
    fun setDebug(debug: Boolean) {
        mDebug = debug
    }

    var isRefreshing: Boolean
        /**
         * is current status refreshing
         *
         * @return
         */
        get() = STATUS.isRefreshing(mStatus)
        /**
         * auto refresh or cancel
         *
         * @param refreshing
         */
        set(refreshing) {
            if (!isRefreshEnabled || mHeaderView == null) {
                return
            }
            mAutoLoading = refreshing
            if (refreshing) {
                if (STATUS.isStatusDefault(mStatus)) {
                    setStatus(STATUS.STATUS_SWIPING_TO_REFRESH)
                    scrollDefaultToRefreshing()
                }
            } else {
                if (STATUS.isRefreshing(mStatus)) {
                    mRefreshCallback.onComplete()
                    postDelayed({ scrollRefreshingToDefault() }, mRefreshCompleteDelayDuration.toLong())
                }
            }
        }
    var isLoadingMore: Boolean
        /**
         * is current status loading more
         *
         * @return
         */
        get() = STATUS.isLoadingMore(mStatus)
        /**
         * auto loading more or cancel
         *
         * @param loadingMore
         */
        set(loadingMore) {
            if (!isLoadMoreEnabled || mFooterView == null) {
                return
            }
            mAutoLoading = loadingMore
            if (loadingMore) {
                if (STATUS.isStatusDefault(mStatus)) {
                    setStatus(STATUS.STATUS_SWIPING_TO_LOAD_MORE)
                    scrollDefaultToLoadingMore()
                }
            } else {
                if (STATUS.isLoadingMore(mStatus)) {
                    mLoadMoreCallback.onComplete()
                    postDelayed({ scrollLoadingMoreToDefault() }, mLoadMoreCompleteDelayDuration.toLong())
                }
            }
        }

    /**
     * set refresh header view, the view must at lease be an implement of `SwipeRefreshTrigger`.
     * the view can also implement `SwipeTrigger` for more extension functions
     *
     * @param view
     */
    fun setRefreshHeaderView(view: View) {
        if (view is SwipeRefreshTrigger) {
            if (mHeaderView != null && mHeaderView !== view) {
                removeView(mHeaderView)
            }
            if (mHeaderView !== view) {
                mHeaderView = view
                addView(view)
            }
        } else {
            Log.e(TAG, "Refresh header view must be an implement of SwipeRefreshTrigger")
        }
    }

    /**
     * set load more footer view, the view must at least be an implement of SwipeLoadTrigger
     * the view can also implement `SwipeTrigger` for more extension functions
     *
     * @param view
     */
    fun setLoadMoreFooterView(view: View) {
        if (view is SwipeLoadMoreTrigger) {
            if (mFooterView != null && mFooterView !== view) {
                removeView(mFooterView)
            }
            if (mFooterView !== view) {
                mFooterView = view
                addView(mFooterView)
            }
        } else {
            Log.e(TAG, "Load more footer view must be an implement of SwipeLoadTrigger")
        }
    }

    /**
     * set the style of the refresh header
     *
     * @param style
     */
    fun setSwipeStyle(style: Int) {
        mStyle = style
        requestLayout()
    }

    /**
     * set how hard to drag. bigger easier, smaller harder;
     *
     * @param dragRatio default value is [.DEFAULT_DRAG_RATIO]
     */
    fun setDragRatio(dragRatio: Float) {
        mDragRatio = dragRatio
    }

    /**
     * set the value of [.mRefreshTriggerOffset].
     * Default value is the refresh header view height [.mHeaderHeight]
     *
     *
     * If the offset you set is smaller than [.mHeaderHeight] or not set,
     * using [.mHeaderHeight] as default value
     *
     * @param offset
     */
    fun setRefreshTriggerOffset(offset: Int) {
        mRefreshTriggerOffset = offset.toFloat()
    }

    /**
     * set the value of [.mLoadMoreTriggerOffset].
     * Default value is the load more footer view height [.mFooterHeight]
     *
     *
     * If the offset you set is smaller than [.mFooterHeight] or not set,
     * using [.mFooterHeight] as default value
     *
     * @param offset
     */
    fun setLoadMoreTriggerOffset(offset: Int) {
        mLoadMoreTriggerOffset = offset.toFloat()
    }

    /**
     * Set the final offset you can swipe to refresh.<br></br>
     * If the offset you set is 0(default value) or smaller than [.mRefreshTriggerOffset]
     * there no final offset
     *
     * @param offset
     */
    fun setRefreshFinalDragOffset(offset: Int) {
        mRefreshFinalDragOffset = offset.toFloat()
    }

    /**
     * Set the final offset you can swipe to load more.<br></br>
     * If the offset you set is 0(default value) or smaller than [.mLoadMoreTriggerOffset],
     * there no final offset
     *
     * @param offset
     */
    fun setLoadMoreFinalDragOffset(offset: Int) {
        mLoadMoreFinalDragOffset = offset.toFloat()
    }

    /**
     * set [.mSwipingToRefreshToDefaultScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setSwipingToRefreshToDefaultScrollingDuration(duration: Int) {
        mSwipingToRefreshToDefaultScrollingDuration = duration
    }

    /**
     * set [.mReleaseToRefreshToRefreshingScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setReleaseToRefreshingScrollingDuration(duration: Int) {
        mReleaseToRefreshToRefreshingScrollingDuration = duration
    }

    /**
     * set [.mRefreshCompleteDelayDuration] in milliseconds
     *
     * @param duration
     */
    fun setRefreshCompleteDelayDuration(duration: Int) {
        mRefreshCompleteDelayDuration = duration
    }

    /**
     * set [.mRefreshCompleteToDefaultScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setRefreshCompleteToDefaultScrollingDuration(duration: Int) {
        mRefreshCompleteToDefaultScrollingDuration = duration
    }

    /**
     * set [.mDefaultToRefreshingScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setDefaultToRefreshingScrollingDuration(duration: Int) {
        mDefaultToRefreshingScrollingDuration = duration
    }

    /**
     * set [@mSwipingToLoadMoreToDefaultScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setSwipingToLoadMoreToDefaultScrollingDuration(duration: Int) {
        mSwipingToLoadMoreToDefaultScrollingDuration = duration
    }

    /**
     * set [.mReleaseToLoadMoreToLoadingMoreScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setReleaseToLoadingMoreScrollingDuration(duration: Int) {
        mReleaseToLoadMoreToLoadingMoreScrollingDuration = duration
    }

    /**
     * set [.mLoadMoreCompleteDelayDuration] in milliseconds
     *
     * @param duration
     */
    fun setLoadMoreCompleteDelayDuration(duration: Int) {
        mLoadMoreCompleteDelayDuration = duration
    }

    /**
     * set [.mLoadMoreCompleteToDefaultScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setLoadMoreCompleteToDefaultScrollingDuration(duration: Int) {
        mLoadMoreCompleteToDefaultScrollingDuration = duration
    }

    /**
     * set [.mDefaultToLoadingMoreScrollingDuration] in milliseconds
     *
     * @param duration
     */
    fun setDefaultToLoadingMoreScrollingDuration(duration: Int) {
        mDefaultToLoadingMoreScrollingDuration = duration
    }

    /**
     * set an [OnRefreshListener] to listening refresh event
     *
     * @param listener
     */
    fun setOnRefreshListener(listener: OnRefreshListener?) {
        mRefreshListener = listener
    }

    /**
     * set an [OnLoadMoreListener] to listening load more event
     *
     * @param listener
     */
    fun setOnLoadMoreListener(listener: OnLoadMoreListener?) {
        mLoadMoreListener = listener
    }

    /**
     * copy from [android.support.v4.widget.SwipeRefreshLayout.canChildScrollUp]
     *
     * @return Whether it is possible for the child view of this layout to
     * scroll up. Override this if the child view is a custom view.
     */
    protected fun canChildScrollUp(): Boolean {
        return mTargetView!!.canScrollVertically(-1)
    }

    /**
     * Whether it is possible for the child view of this layout to
     * scroll down. Override this if the child view is a custom view.
     *
     * @return
     */
    protected fun canChildScrollDown(): Boolean {
        return mTargetView!!.canScrollVertically(1)
    }

    /**
     * @see .onLayout
     */
    private fun layoutChildren() {
        val width = measuredWidth
        val height = measuredHeight
        val paddingLeft = paddingLeft
        val paddingTop = paddingTop
        val paddingRight = paddingRight
        val paddingBottom = paddingBottom
        if (mTargetView == null) {
            return
        }

        // layout header
        if (mHeaderView != null) {
            val headerView: View = mHeaderView as View
            val lp = headerView.layoutParams as MarginLayoutParams
            val headerLeft = paddingLeft + lp.leftMargin
            val headerTop: Int = when (mStyle) {
                STYLE.CLASSIC ->                   // classic
                    paddingTop + lp.topMargin - mHeaderHeight + mHeaderOffset

                STYLE.ABOVE ->                     // classic
                    paddingTop + lp.topMargin - mHeaderHeight + mHeaderOffset

                STYLE.BLEW ->                      // blew
                    paddingTop + lp.topMargin

                STYLE.SCALE ->                     // scale
                    paddingTop + lp.topMargin - mHeaderHeight / 2 + mHeaderOffset / 2

                else ->                            // classic
                    paddingTop + lp.topMargin - mHeaderHeight + mHeaderOffset
            }
            val headerRight = headerLeft + headerView.measuredWidth
            val headerBottom = headerTop + headerView.measuredHeight
            headerView.layout(headerLeft, headerTop, headerRight, headerBottom)
        }


        // layout target
        if (mTargetView != null) {
            val targetView: View = mTargetView as View
            val lp = targetView.layoutParams as MarginLayoutParams
            val targetLeft = paddingLeft + lp.leftMargin
            val targetTop: Int = when (mStyle) {
                STYLE.CLASSIC ->                   // classic
                    paddingTop + lp.topMargin + mTargetOffset

                STYLE.ABOVE ->                     // above
                    paddingTop + lp.topMargin

                STYLE.BLEW ->                      // classic
                    paddingTop + lp.topMargin + mTargetOffset

                STYLE.SCALE ->                     // classic
                    paddingTop + lp.topMargin + mTargetOffset

                else ->                            // classic
                    paddingTop + lp.topMargin + mTargetOffset
            }
            val targetRight = targetLeft + targetView.measuredWidth
            val targetBottom = targetTop + targetView.measuredHeight
            targetView.layout(targetLeft, targetTop, targetRight, targetBottom)
        }

        // layout footer
        if (mFooterView != null) {
            val footerView: View = mFooterView as View
            val lp = footerView.layoutParams as MarginLayoutParams
            val footerLeft = paddingLeft + lp.leftMargin
            val footerBottom: Int = when (mStyle) {
                STYLE.CLASSIC ->                   // classic
                    height - paddingBottom - lp.bottomMargin + mFooterHeight + mFooterOffset

                STYLE.ABOVE ->                     // classic
                    height - paddingBottom - lp.bottomMargin + mFooterHeight + mFooterOffset

                STYLE.BLEW ->                      // blew
                    height - paddingBottom - lp.bottomMargin

                STYLE.SCALE ->                     // scale
                    height - paddingBottom - lp.bottomMargin + mFooterHeight / 2 + mFooterOffset / 2

                else ->                            // classic
                    height - paddingBottom - lp.bottomMargin + mFooterHeight + mFooterOffset
            }
            val footerTop = footerBottom - footerView.measuredHeight
            val footerRight = footerLeft + footerView.measuredWidth
            footerView.layout(footerLeft, footerTop, footerRight, footerBottom)
        }
        if (mStyle == STYLE.CLASSIC || mStyle == STYLE.ABOVE) {
            if (mHeaderView != null) {
                mHeaderView?.bringToFront()
            }
            if (mFooterView != null) {
                mFooterView?.bringToFront()
            }
        } else if (mStyle == STYLE.BLEW || mStyle == STYLE.SCALE) {
            if (mTargetView != null) {
                mTargetView?.bringToFront()
            }
        }
    }

    private fun fixCurrentStatusLayout() {
        when {
            STATUS.isRefreshing(mStatus) -> {
                mTargetOffset = (mRefreshTriggerOffset + 0.5f).toInt()
                mHeaderOffset = mTargetOffset
                mFooterOffset = 0
                layoutChildren()
                invalidate()
            }

            STATUS.isStatusDefault(mStatus) -> {
                mTargetOffset = 0
                mHeaderOffset = 0
                mFooterOffset = 0
                layoutChildren()
                invalidate()
            }

            STATUS.isLoadingMore(mStatus) -> {
                mTargetOffset = -(mLoadMoreTriggerOffset + 0.5f).toInt()
                mHeaderOffset = 0
                mFooterOffset = mTargetOffset
                layoutChildren()
                invalidate()
            }
        }
    }

    /**
     * scrolling by physical touch with your fingers
     *
     * @param yDiff
     */
    private fun fingerScroll(yDiff: Float) {
        val ratio = mDragRatio
        var yScrolled = yDiff * ratio

        // make sure (targetOffset>0 -> targetOffset=0 -> default status)
        // or (targetOffset<0 -> targetOffset=0 -> default status)
        // forbidden fling (targetOffset>0 -> targetOffset=0 ->targetOffset<0 -> default status)
        // or (targetOffset<0 -> targetOffset=0 ->targetOffset>0 -> default status)
        // I am so smart :)
        val tmpTargetOffset = yScrolled + mTargetOffset
        if (tmpTargetOffset > 0 && mTargetOffset < 0 || tmpTargetOffset < 0 && mTargetOffset > 0) {
            yScrolled = -mTargetOffset.toFloat()
        }
        if (mRefreshFinalDragOffset >= mRefreshTriggerOffset && tmpTargetOffset > mRefreshFinalDragOffset) {
            yScrolled = mRefreshFinalDragOffset - mTargetOffset
        } else if (mLoadMoreFinalDragOffset >= mLoadMoreTriggerOffset && -tmpTargetOffset > mLoadMoreFinalDragOffset) {
            yScrolled = -mLoadMoreFinalDragOffset - mTargetOffset
        }
        if (STATUS.isRefreshStatus(mStatus)) {
            mRefreshCallback.onMove(mTargetOffset, false, false)
        } else if (STATUS.isLoadMoreStatus(mStatus)) {
            mLoadMoreCallback.onMove(mTargetOffset, false, false)
        }
        updateScroll(yScrolled)
    }

    private fun autoScroll(yScrolled: Float) {
        when {
            STATUS.isSwipingToRefresh(mStatus) -> {
                mRefreshCallback.onMove(mTargetOffset, false, true)
            }

            STATUS.isReleaseToRefresh(mStatus) -> {
                mRefreshCallback.onMove(mTargetOffset, false, true)
            }

            STATUS.isRefreshing(mStatus) -> {
                mRefreshCallback.onMove(mTargetOffset, true, true)
            }

            STATUS.isSwipingToLoadMore(mStatus) -> {
                mLoadMoreCallback.onMove(mTargetOffset, false, true)
            }

            STATUS.isReleaseToLoadMore(mStatus) -> {
                mLoadMoreCallback.onMove(mTargetOffset, false, true)
            }

            STATUS.isLoadingMore(mStatus) -> {
                mLoadMoreCallback.onMove(mTargetOffset, true, true)
            }
        }
        updateScroll(yScrolled)
    }

    /**
     * Process the scrolling(auto or physical) and append the diff values to mTargetOffset
     * I think it's the most busy and core method. :) a ha ha ha ha...
     *
     * @param yScrolled
     */
    private fun updateScroll(yScrolled: Float) {
        if (yScrolled == 0f) {
            return
        }
        mTargetOffset += yScrolled.toInt()
        if (STATUS.isRefreshStatus(mStatus)) {
            mHeaderOffset = mTargetOffset
            mFooterOffset = 0
        } else if (STATUS.isLoadMoreStatus(mStatus)) {
            mFooterOffset = mTargetOffset
            mHeaderOffset = 0
        }
        if (mDebug) {
            Log.i(TAG, "mTargetOffset = $mTargetOffset")
        }
        layoutChildren()
        invalidate()
    }

    /**
     * on active finger up
     */
    private fun onActivePointerUp() {
        when {
            STATUS.isSwipingToRefresh(mStatus) -> {
                // simply return
                scrollSwipingToRefreshToDefault()
            }

            STATUS.isSwipingToLoadMore(mStatus) -> {
                // simply return
                scrollSwipingToLoadMoreToDefault()
            }

            STATUS.isReleaseToRefresh(mStatus) -> {
                // return to header height and perform refresh
                mRefreshCallback.onRelease()
                scrollReleaseToRefreshToRefreshing()
            }

            STATUS.isReleaseToLoadMore(mStatus) -> {
                // return to footer height and perform loadMore
                mLoadMoreCallback.onRelease()
                scrollReleaseToLoadMoreToLoadingMore()
            }
        }
    }

    /**
     * on not active finger up
     *
     * @param ev
     */
    private fun onSecondaryPointerUp(ev: MotionEvent) {
        val pointerIndex = ev.actionIndex
        val pointerId = ev.getPointerId(pointerIndex)
        if (pointerId == mActivePointerId) {
            // This was our active pointer going up. Choose a new
            // active pointer and adjust accordingly.
            val newPointerIndex = if (pointerIndex == 0) 1 else 0
            mActivePointerId = ev.getPointerId(newPointerIndex)
        }
    }

    private fun scrollDefaultToRefreshing() {
        mAutoScroller.autoScroll((mRefreshTriggerOffset + 0.5f).toInt(), mDefaultToRefreshingScrollingDuration)
    }

    private fun scrollDefaultToLoadingMore() {
        mAutoScroller.autoScroll(-(mLoadMoreTriggerOffset + 0.5f).toInt(), mDefaultToLoadingMoreScrollingDuration)
    }

    private fun scrollSwipingToRefreshToDefault() {
        mAutoScroller.autoScroll(-mHeaderOffset, mSwipingToRefreshToDefaultScrollingDuration)
    }

    private fun scrollSwipingToLoadMoreToDefault() {
        mAutoScroller.autoScroll(-mFooterOffset, mSwipingToLoadMoreToDefaultScrollingDuration)
    }

    private fun scrollReleaseToRefreshToRefreshing() {
        mAutoScroller.autoScroll(mHeaderHeight - mHeaderOffset, mReleaseToRefreshToRefreshingScrollingDuration)
    }

    private fun scrollReleaseToLoadMoreToLoadingMore() {
        mAutoScroller.autoScroll(-mFooterOffset - mFooterHeight, mReleaseToLoadMoreToLoadingMoreScrollingDuration)
    }

    private fun scrollRefreshingToDefault() {
        mAutoScroller.autoScroll(-mHeaderOffset, mRefreshCompleteToDefaultScrollingDuration)
    }

    private fun scrollLoadingMoreToDefault() {
        mAutoScroller.autoScroll(-mFooterOffset, mLoadMoreCompleteToDefaultScrollingDuration)
    }

    /**
     * invoke when [AutoScroller.finish] is automatic
     */
    private fun autoScrollFinished() {
        val mLastStatus = mStatus
        if (STATUS.isReleaseToRefresh(mStatus)) {
            setStatus(STATUS.STATUS_REFRESHING)
            fixCurrentStatusLayout()
            mRefreshCallback.onRefresh()
        } else if (STATUS.isRefreshing(mStatus)) {
            setStatus(STATUS.STATUS_DEFAULT)
            fixCurrentStatusLayout()
            mRefreshCallback.onReset()
        } else if (STATUS.isSwipingToRefresh(mStatus)) {
            if (mAutoLoading) {
                mAutoLoading = false
                setStatus(STATUS.STATUS_REFRESHING)
                fixCurrentStatusLayout()
                mRefreshCallback.onRefresh()
            } else {
                setStatus(STATUS.STATUS_DEFAULT)
                fixCurrentStatusLayout()
                mRefreshCallback.onReset()
            }
        } else if (STATUS.isStatusDefault(mStatus)) {
        } else if (STATUS.isSwipingToLoadMore(mStatus)) {
            if (mAutoLoading) {
                mAutoLoading = false
                setStatus(STATUS.STATUS_LOADING_MORE)
                fixCurrentStatusLayout()
                mLoadMoreCallback.onLoadMore()
            } else {
                setStatus(STATUS.STATUS_DEFAULT)
                fixCurrentStatusLayout()
                mLoadMoreCallback.onReset()
            }
        } else if (STATUS.isLoadingMore(mStatus)) {
            setStatus(STATUS.STATUS_DEFAULT)
            fixCurrentStatusLayout()
            mLoadMoreCallback.onReset()
        } else if (STATUS.isReleaseToLoadMore(mStatus)) {
            setStatus(STATUS.STATUS_LOADING_MORE)
            fixCurrentStatusLayout()
            mLoadMoreCallback.onLoadMore()
        } else {
            throw IllegalStateException("illegal state: " + STATUS.getStatus(mStatus))
        }
        if (mDebug) {
            Log.i(TAG, STATUS.getStatus(mLastStatus) + " -> " + STATUS.getStatus(mStatus))
        }
    }

    /**
     * check if it can refresh
     *
     * @return
     */
    private fun onCheckCanRefresh(): Boolean {
        return isRefreshEnabled && !canChildScrollUp() && mHasHeaderView && mRefreshTriggerOffset > 0
    }

    /**
     * check if it can load more
     *
     * @return
     */
    private fun onCheckCanLoadMore(): Boolean {
        return isLoadMoreEnabled && !canChildScrollDown() && mHasFooterView && mLoadMoreTriggerOffset > 0
    }

    private fun getMotionEventY(event: MotionEvent, activePointerId: Int): Float {
        val index = event.findPointerIndex(activePointerId)
        return if (index < 0) {
            INVALID_COORDINATE.toFloat()
        } else event.getY(index)
    }

    private fun getMotionEventX(event: MotionEvent, activePointId: Int): Float {
        val index = event.findPointerIndex(activePointId)
        return if (index < 0) {
            INVALID_COORDINATE.toFloat()
        } else event.getX(index)
    }

    var mRefreshCallback: RefreshCallback = object : RefreshCallback() {
        override fun onPrepare() {
            if (mHeaderView != null && mHeaderView is SwipeTrigger && STATUS.isStatusDefault(mStatus)) {
                (mHeaderView as View).visibility = VISIBLE
                (mHeaderView as SwipeTrigger).onPrepare()
                if (mRefreshListener != null) {
                    mRefreshListener!!.onPrepare()
                }
            }
        }

        override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
            if (mHeaderView != null && mHeaderView is SwipeTrigger && STATUS.isRefreshStatus(mStatus)) {
                if ((mHeaderView as View).visibility != VISIBLE) {
                    (mHeaderView as View).visibility = VISIBLE
                }
                (mHeaderView as SwipeTrigger).onMove(y, isComplete, automatic)
            }
        }

        override fun onRelease() {
            if (mHeaderView != null && mHeaderView is SwipeTrigger && STATUS.isReleaseToRefresh(mStatus)) {
                (mHeaderView as SwipeTrigger).onRelease()
            }
        }

        override fun onRefresh() {
            if (mHeaderView != null && STATUS.isRefreshing(mStatus)) {
                if (mHeaderView is SwipeRefreshTrigger) {
                    (mHeaderView as SwipeRefreshTrigger).onRefresh()
                }
                if (mRefreshListener != null) {
                    mRefreshListener!!.onRefresh()
                }
            }
        }

        override fun onComplete() {
            if (mHeaderView != null && mHeaderView is SwipeTrigger) {
                (mHeaderView as SwipeTrigger).onComplete()
            }
        }

        override fun onReset() {
            if (mHeaderView != null && mHeaderView is SwipeTrigger && STATUS.isStatusDefault(mStatus)) {
                (mHeaderView as SwipeTrigger).onReset()
                (mHeaderView as View).visibility = GONE
                if (mRefreshListener != null) {
                    mRefreshListener?.onReset()
                }
            }
        }

        override fun onCancel() {
            if (mHeaderView != null && mHeaderView is SwipeTrigger && STATUS.isStatusDefault(mStatus)) {
                (mHeaderView as View).visibility = GONE
                (mHeaderView as SwipeTrigger).onCancel()
                if (mRefreshListener != null) {
                    mRefreshListener?.onCancel()
                }
            }
        }
    }
    var mLoadMoreCallback: LoadMoreCallback = object : LoadMoreCallback() {
        override fun onPrepare() {
            if (mFooterView != null && mFooterView is SwipeTrigger && STATUS.isStatusDefault(mStatus)) {
                (mFooterView as View).visibility = VISIBLE
                (mFooterView as SwipeTrigger).onPrepare()
            }
        }

        override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
            if (mFooterView != null && mFooterView is SwipeTrigger && STATUS.isLoadMoreStatus(mStatus)) {
                if ((mFooterView as View).getVisibility() != VISIBLE) {
                    (mFooterView as View).visibility = VISIBLE
                }
                (mFooterView as SwipeTrigger).onMove(y, isComplete, automatic)
            }
        }

        override fun onRelease() {
            if (mFooterView != null && mFooterView is SwipeTrigger && STATUS.isReleaseToLoadMore(mStatus)) {
                (mFooterView as SwipeTrigger).onRelease()
            }
        }

        override fun onLoadMore() {
            if (mFooterView != null && STATUS.isLoadingMore(mStatus)) {
                if (mFooterView is SwipeLoadMoreTrigger) {
                    (mFooterView as SwipeLoadMoreTrigger).onLoadMore()
                }
                if (mLoadMoreListener != null) {
                    mLoadMoreListener!!.onLoadMore()
                }
            }
        }

        override fun onComplete() {
            if (mFooterView != null && mFooterView is SwipeTrigger) {
                (mFooterView as SwipeTrigger).onComplete()
            }
        }

        override fun onReset() {
            if (mFooterView != null && mFooterView is SwipeTrigger && STATUS.isStatusDefault(mStatus)) {
                (mFooterView as SwipeTrigger).onReset()
                (mFooterView as View).visibility = GONE
            }
        }

        override fun onCancel() {
            if (mFooterView != null && mFooterView is SwipeTrigger) {
                (mFooterView as SwipeTrigger).onCancel()
                (mFooterView as View).visibility = GONE
            }
        }
    }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.SwipeToLoadLayout, defStyleAttr, 0)
        try {
            val N = a.indexCount
            for (i in 0 until N) {
                val attr = a.getIndex(i)
                when (attr) {
                    R.styleable.SwipeToLoadLayout_refresh_enabled -> {
                        isRefreshEnabled = a.getBoolean(attr, true)
                    }

                    R.styleable.SwipeToLoadLayout_load_more_enabled -> {
                        isLoadMoreEnabled = a.getBoolean(attr, true)
                    }

                    R.styleable.SwipeToLoadLayout_swipe_style -> {
                        setSwipeStyle(a.getInt(attr, STYLE.CLASSIC))
                    }

                    R.styleable.SwipeToLoadLayout_drag_ratio -> {
                        setDragRatio(a.getFloat(attr, DEFAULT_DRAG_RATIO))
                    }

                    R.styleable.SwipeToLoadLayout_refresh_final_drag_offset -> {
                        setRefreshFinalDragOffset(a.getDimensionPixelOffset(attr, 0))
                    }

                    R.styleable.SwipeToLoadLayout_load_more_final_drag_offset -> {
                        setLoadMoreFinalDragOffset(a.getDimensionPixelOffset(attr, 0))
                    }

                    R.styleable.SwipeToLoadLayout_refresh_trigger_offset -> {
                        setRefreshTriggerOffset(a.getDimensionPixelOffset(attr, 0))
                    }

                    R.styleable.SwipeToLoadLayout_load_more_trigger_offset -> {
                        setLoadMoreTriggerOffset(a.getDimensionPixelOffset(attr, 0))
                    }

                    R.styleable.SwipeToLoadLayout_swiping_to_refresh_to_default_scrolling_duration -> {
                        setSwipingToRefreshToDefaultScrollingDuration(a.getInt(attr, DEFAULT_SWIPING_TO_REFRESH_TO_DEFAULT_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_release_to_refreshing_scrolling_duration -> {
                        setReleaseToRefreshingScrollingDuration(a.getInt(attr, DEFAULT_RELEASE_TO_REFRESHING_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_refresh_complete_delay_duration -> {
                        setRefreshCompleteDelayDuration(a.getInt(attr, DEFAULT_REFRESH_COMPLETE_DELAY_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_refresh_complete_to_default_scrolling_duration -> {
                        setRefreshCompleteToDefaultScrollingDuration(a.getInt(attr, DEFAULT_REFRESH_COMPLETE_TO_DEFAULT_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_default_to_refreshing_scrolling_duration -> {
                        setDefaultToRefreshingScrollingDuration(a.getInt(attr, DEFAULT_DEFAULT_TO_REFRESHING_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_swiping_to_load_more_to_default_scrolling_duration -> {
                        setSwipingToLoadMoreToDefaultScrollingDuration(a.getInt(attr, DEFAULT_SWIPING_TO_LOAD_MORE_TO_DEFAULT_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_release_to_loading_more_scrolling_duration -> {
                        setReleaseToLoadingMoreScrollingDuration(a.getInt(attr, DEFAULT_RELEASE_TO_LOADING_MORE_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_load_more_complete_delay_duration -> {
                        setLoadMoreCompleteDelayDuration(a.getInt(attr, DEFAULT_LOAD_MORE_COMPLETE_DELAY_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_load_more_complete_to_default_scrolling_duration -> {
                        setLoadMoreCompleteToDefaultScrollingDuration(a.getInt(attr, DEFAULT_LOAD_MORE_COMPLETE_TO_DEFAULT_SCROLLING_DURATION))
                    }

                    R.styleable.SwipeToLoadLayout_default_to_loading_more_scrolling_duration -> {
                        setDefaultToLoadingMoreScrollingDuration(a.getInt(attr, DEFAULT_DEFAULT_TO_LOADING_MORE_SCROLLING_DURATION))
                    }
                }
            }
        } finally {
            a.recycle()
        }
        mTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
        mAutoScroller = AutoScroller()
    }

    /**
     * refresh event callback
     */
    abstract inner class RefreshCallback : SwipeTrigger, SwipeRefreshTrigger

    /**
     * load more event callback
     */
    abstract inner class LoadMoreCallback : SwipeTrigger, SwipeLoadMoreTrigger
    private inner class AutoScroller : Runnable {
        private val mScroller: Scroller = Scroller(context)
        private var mmLastY = 0
        private var mRunning = false
        private var mAbort = false

        override fun run() {
            val finish = !mScroller.computeScrollOffset() || mScroller.isFinished
            val currY = mScroller.currY
            val yDiff = currY - mmLastY
            if (finish) {
                finish()
            } else {
                mmLastY = currY
                this@SwipeToLoadLayout.autoScroll(yDiff.toFloat())
                post(this)
            }
        }

        /**
         * remove the post callbacks and reset default values
         */
        private fun finish() {
            mmLastY = 0
            mRunning = false
            removeCallbacks(this)
            // if abort by user, don't call
            if (!mAbort) {
                autoScrollFinished()
            }
        }

        /**
         * abort scroll if it is scrolling
         */
        fun abortIfRunning() {
            if (mRunning) {
                if (!mScroller.isFinished) {
                    mAbort = true
                    mScroller.forceFinished(true)
                }
                finish()
                mAbort = false
            }
        }

        /**
         * The param yScrolled here isn't final pos of y.
         * It's just like the yScrolled param in the
         * [.updateScroll]
         *
         * @param yScrolled
         * @param duration
         */
        fun autoScroll(yScrolled: Int, duration: Int) {
            removeCallbacks(this)
            mmLastY = 0
            if (!mScroller.isFinished) {
                mScroller.forceFinished(true)
            }
            mScroller.startScroll(0, 0, 0, yScrolled, duration)
            post(this)
            mRunning = true
        }
    }

    /**
     * Set the current status for better control
     *
     * @param status
     */
    private fun setStatus(status: Int) {
        mStatus = status
        if (mDebug) {
            STATUS.printStatus(status)
        }
    }

    /**
     * an inner util class.
     * enum of status
     */
    private object STATUS {
        private const val STATUS_REFRESH_RETURNING = -4
        const val STATUS_REFRESHING = -3
        const val STATUS_RELEASE_TO_REFRESH = -2
        const val STATUS_SWIPING_TO_REFRESH = -1
        const val STATUS_DEFAULT = 0
        const val STATUS_SWIPING_TO_LOAD_MORE = 1
        const val STATUS_RELEASE_TO_LOAD_MORE = 2
        const val STATUS_LOADING_MORE = 3
        private const val STATUS_LOAD_MORE_RETURNING = 4
        fun isRefreshing(status: Int): Boolean {
            return status == STATUS_REFRESHING
        }

        fun isLoadingMore(status: Int): Boolean {
            return status == STATUS_LOADING_MORE
        }

        fun isReleaseToRefresh(status: Int): Boolean {
            return status == STATUS_RELEASE_TO_REFRESH
        }

        fun isReleaseToLoadMore(status: Int): Boolean {
            return status == STATUS_RELEASE_TO_LOAD_MORE
        }

        fun isSwipingToRefresh(status: Int): Boolean {
            return status == STATUS_SWIPING_TO_REFRESH
        }

        fun isSwipingToLoadMore(status: Int): Boolean {
            return status == STATUS_SWIPING_TO_LOAD_MORE
        }

        fun isRefreshStatus(status: Int): Boolean {
            return status < STATUS_DEFAULT
        }

        fun isLoadMoreStatus(status: Int): Boolean {
            return status > STATUS_DEFAULT
        }

        fun isStatusDefault(status: Int): Boolean {
            return status == STATUS_DEFAULT
        }

        fun getStatus(status: Int): String {
            val statusInfo: String = when (status) {
                STATUS_REFRESH_RETURNING -> "status_refresh_returning"
                STATUS_REFRESHING -> "status_refreshing"
                STATUS_RELEASE_TO_REFRESH -> "status_release_to_refresh"
                STATUS_SWIPING_TO_REFRESH -> "status_swiping_to_refresh"
                STATUS_DEFAULT -> "status_default"
                STATUS_SWIPING_TO_LOAD_MORE -> "status_swiping_to_load_more"
                STATUS_RELEASE_TO_LOAD_MORE -> "status_release_to_load_more"
                STATUS_LOADING_MORE -> "status_loading_more"
                STATUS_LOAD_MORE_RETURNING -> "status_load_more_returning"
                else -> "status_illegal!"
            }
            return statusInfo
        }

        fun printStatus(status: Int) {
            Log.i(TAG, "printStatus:" + getStatus(status))
        }
    }

}