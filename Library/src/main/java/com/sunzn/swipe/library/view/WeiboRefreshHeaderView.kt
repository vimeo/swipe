package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class WeiboRefreshHeaderView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context!!, attrs, defStyleAttr) {

    private val mHeaderHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_60)
    private lateinit var icon: AppCompatImageView
    private lateinit var text: AppCompatTextView
    private lateinit var bar: ProgressBar
    private val rotateUp: Animation
    private val rotateDown: Animation
    private var rotated = false

    init {
        rotateUp = AnimationUtils.loadAnimation(context, R.anim.rotate_up)
        rotateDown = AnimationUtils.loadAnimation(context, R.anim.rotate_down)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        text = findViewById(R.id.swipe_weibo_text)
        icon = findViewById(R.id.swipe_weibo_icon)
        bar = findViewById(R.id.swipe_weibo_bar)
    }

    override fun onRefresh() {
        icon.clearAnimation()
        icon.visibility = GONE
        bar.visibility = VISIBLE
        text.text = "正在刷新"
    }

    override fun onPrepare() {
        Log.d("WeiboRefreshHeaderView", "onPrepare()")
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            icon.visibility = VISIBLE
            bar.visibility = GONE
            if (y > mHeaderHeight) {
                text.text = "释放刷新"
                if (!rotated) {
                    icon.clearAnimation()
                    icon.startAnimation(rotateUp)
                    rotated = true
                }
            } else if (y < mHeaderHeight) {
                if (rotated) {
                    icon.clearAnimation()
                    icon.startAnimation(rotateDown)
                    rotated = false
                }
                text.text = "下拉刷新"
            }
        }
    }

    override fun onRelease() {
        Log.d("WeiboRefreshHeaderView", "onRelease()")
    }

    override fun onComplete() {
        rotated = false
        icon.clearAnimation()
        icon.visibility = GONE
        bar.visibility = GONE
        text.text = "刷新完成"
    }

    override fun onReset() {
        rotated = false
        icon.clearAnimation()
        icon.visibility = GONE
        bar.visibility = GONE
        text.text = "下拉刷新"
    }

}