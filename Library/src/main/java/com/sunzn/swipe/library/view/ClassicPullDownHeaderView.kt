package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

/**
 * Created by sunzn on 2016/5/30.
 */
class ClassicPullDownHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context, attrs, defStyleAttr) {

    private lateinit var arrow: AppCompatImageView
    private lateinit var success: AppCompatImageView
    private lateinit var text: AppCompatTextView
    private lateinit var progressBar: ProgressBar

    private val mHeaderHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_55)

    private val rotateUp: Animation
    private val rotateDown: Animation
    private var rotated = false

    init {
        rotateUp = AnimationUtils.loadAnimation(context, R.anim.rotate_up)
        rotateDown = AnimationUtils.loadAnimation(context, R.anim.rotate_down)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        text = findViewById(R.id.swipe_classic_text)
        arrow = findViewById(R.id.swipe_classic_arrow)
        success = findViewById(R.id.swipe_classic_success)
        progressBar = findViewById(R.id.progressbar)
    }

    override fun onRefresh() {
        success.visibility = GONE
        arrow.clearAnimation()
        arrow.visibility = GONE
        progressBar.visibility = VISIBLE
        text.text = "正在刷新"
    }

    override fun onPrepare() {
        Log.d("TwitterRefreshHeader", "onPrepare()")
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            arrow.visibility = VISIBLE
            progressBar.visibility = GONE
            success.visibility = GONE
            if (y > mHeaderHeight) {
                text.text = "释放刷新"
                if (!rotated) {
                    arrow.clearAnimation()
                    arrow.startAnimation(rotateUp)
                    rotated = true
                }
            } else if (y < mHeaderHeight) {
                if (rotated) {
                    arrow.clearAnimation()
                    arrow.startAnimation(rotateDown)
                    rotated = false
                }
                text.text = "下拉刷新"
            }
        }
    }

    override fun onRelease() {
        Log.d("TwitterRefreshHeader", "onRelease()")
    }

    override fun onComplete() {
        rotated = false
        success.visibility = VISIBLE
        arrow.clearAnimation()
        arrow.visibility = GONE
        progressBar.visibility = GONE
        text.text = "刷新完毕"
    }

    override fun onReset() {
        rotated = false
        success.visibility = GONE
        arrow.clearAnimation()
        arrow.visibility = GONE
        progressBar.visibility = GONE
    }

}