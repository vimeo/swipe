package com.sunzn.swipe.library

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * Created by sunzn on 2016/5/30.
 */
open class SwipeRefreshHeaderLayout : FrameLayout, SwipeRefreshTrigger, SwipeTrigger {

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

    }

    override fun onRefresh() {}
    override fun onPrepare() {}
    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {}
    override fun onRelease() {}
    override fun onComplete() {}
    override fun onReset() {}
    override fun onCancel() {}
}