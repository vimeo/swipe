package com.sunzn.swipe.library

abstract class OnRefreshListenerAdapter : OnRefreshListener {
    override fun onPrepare() {}
    override fun onRefresh() {}
    override fun onReset() {}
    override fun onCancel() {}
}