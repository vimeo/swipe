package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class WeatherRefreshHeaderView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context!!, attrs, defStyleAttr) {

    private val mHeaderHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_80)
    private val rotateAnimation: Animation
    private var rotated = false
    private lateinit var icon: AppCompatImageView
    private lateinit var text: AppCompatTextView

    init {
        rotateAnimation = AnimationUtils.loadAnimation(context, R.anim.rotate_sun)
        rotateAnimation.interpolator = LinearInterpolator()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        text = findViewById(R.id.swipe_sun_text)
        icon = findViewById(R.id.swipe_sun_icon)
    }

    override fun onRefresh() {
        icon.clearAnimation()
        icon.startAnimation(rotateAnimation)
        text.text = "正在刷新"
    }

    override fun onPrepare() {
        rotated = false
        icon.clearAnimation()
        text.text = "下拉刷新"
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            if (y > mHeaderHeight) {
                text.text = "释放刷新"
                if (!rotated) {
                    icon.clearAnimation()
                    icon.startAnimation(rotateAnimation)
                    rotated = true
                }
            } else if (y < mHeaderHeight) {
                if (rotated) {
                    rotated = false
                }
                text.text = "下拉刷新"
            }
        }
    }

    override fun onRelease() {
        Log.d("WeatherRefreshHeader", "onRelease()")
    }

    override fun onComplete() {
        rotated = false
        text.text = "刷新完成"
        icon.clearAnimation()
    }

    override fun onReset() {
        rotated = false
        text.text = "下拉刷新"
    }

}