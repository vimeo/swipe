package com.sunzn.swipe.library.view

import android.animation.Keyframe
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout
import java.util.Random

class CookRefreshHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context, attrs, defStyleAttr) {

    private val mHeaderHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_90)
    private var rotated = false
    private val mHandler = Handler()
    private lateinit var meat: AppCompatImageView
    private lateinit var pot: AppCompatImageView
    private lateinit var rice: AppCompatImageView
    private lateinit var radish: AppCompatImageView
    private lateinit var cover: RelativeLayout
    private lateinit var pan: AppCompatImageView

    override fun onFinishInflate() {
        super.onFinishInflate()
        meat = findViewById(R.id.swipe_cook_meat)
        pot = findViewById(R.id.swipe_cook_pot)
        rice = findViewById(R.id.swipe_cook_rice)
        radish = findViewById(R.id.swipe_cook_radish)
        cover = findViewById(R.id.swipe_cook_cover)
        pan = findViewById(R.id.swipe_cook_pan)
    }

    override fun onRefresh() {
        //正在刷新,执行动画
        cover.visibility = INVISIBLE
        meat.visibility = VISIBLE
        pot.visibility = VISIBLE
        rice.visibility = VISIBLE
        radish.visibility = VISIBLE
        cover.alpha = 0f
        startAnimation()
    }

    override fun onPrepare() {
        Log.d("CookRefreshHeaderView", "onPrepare()")
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            if (y >= mHeaderHeight) {
                if (!rotated) {
                    rotated = true
                }
            } else {
                val tan = (y - mHeaderHeight * 2 / 3).toFloat() / cover.width.toFloat()
                var angle = (tan * 90).toInt()
                if (angle > 30) {
                    angle = 30
                }
                if (angle < 0) {
                    angle = 0
                }
                cover.rotation = -angle.toFloat()
            }
        }
    }

    override fun onRelease() {
        Log.d("CookRefreshHeaderView", "onRelease()")
    }

    override fun onComplete() {
        rotated = false
        mHandler.removeCallbacksAndMessages(null)
        cover.visibility = VISIBLE
        meat.visibility = GONE
        pot.visibility = GONE
        rice.visibility = GONE
        radish.visibility = GONE
        cover.alpha = 1f
        cover.rotation = 0f
        val defaultPoint = floatArrayOf(0f, 0f)
        startParabolaAnimation(meat, defaultPoint, defaultPoint, defaultPoint)
        startParabolaAnimation(pot, defaultPoint, defaultPoint, defaultPoint)
        startParabolaAnimation(rice, defaultPoint, defaultPoint, defaultPoint)
        startParabolaAnimation(radish, defaultPoint, defaultPoint, defaultPoint)
    }

    override fun onReset() {
        rotated = false
    }

    private fun startAnimation() {
        val x = 0f
        val y = 0f
        val startPoint = floatArrayOf(x, y)
        val random = Random()
        var nextInt = random.nextInt(50)
        val endPoint = floatArrayOf((nextInt + 180).toFloat(), (nextInt + 30).toFloat())
        val midPoint = floatArrayOf((nextInt + 100).toFloat(), (nextInt - 70).toFloat())
        nextInt = random.nextInt(40)
        val endPoint2 = floatArrayOf((nextInt + 160).toFloat(), (nextInt + 40).toFloat())
        val midPoint2 = floatArrayOf((nextInt + 80).toFloat(), (nextInt - 80).toFloat())
        nextInt = random.nextInt(30)
        val endPoint3 = floatArrayOf((nextInt - 200).toFloat(), (nextInt + 40).toFloat())
        val midPoint3 = floatArrayOf((nextInt - 100).toFloat(), (nextInt - 70).toFloat())
        nextInt = random.nextInt(60)
        val endPoint4 = floatArrayOf((nextInt - 170).toFloat(), (nextInt + 45).toFloat())
        val midPoint4 = floatArrayOf((nextInt - 80).toFloat(), (nextInt - 80).toFloat())
        mHandler.postDelayed({ startParabolaAnimation(meat, startPoint, endPoint, midPoint) }, 100)
        mHandler.postDelayed({ startParabolaAnimation(pot, startPoint, endPoint2, midPoint2) }, 200)
        mHandler.postDelayed({ startParabolaAnimation(rice, startPoint, endPoint3, midPoint3) }, 300)
        mHandler.postDelayed({ startParabolaAnimation(radish, startPoint, endPoint4, midPoint4) }, 400)
        mHandler.postDelayed({ startAnimation() }, 500)
    }

    companion object {
        /**
         * 抛物线动画
         *
         * @param view
         * @param startPoint 起点坐标
         * @param endPoint   结束点坐标
         * @param midPoint   中间点坐标
         * @return
         */
        fun startParabolaAnimation(view: View?, startPoint: FloatArray, endPoint: FloatArray, midPoint: FloatArray): ObjectAnimator {
            //分300帧完成动画
            val count = 200
            //动画时间持续1.5秒
            val duration = 600
            val keyframes = arrayOfNulls<Keyframe>(count)
            val keyStep = 1f / count.toFloat()
            var key = keyStep
            //计算并保存每一帧x轴的位置
            for (i in 0 until count) {
                keyframes[i] = Keyframe.ofFloat(key, i * getDx(startPoint, endPoint) / count + startPoint[0])
                key += keyStep
            }
            val pvhX = PropertyValuesHolder.ofKeyframe("translationX", *keyframes)
            key = keyStep
            //计算并保存每一帧y轴的位置
            for (i in 0 until count) {
                keyframes[i] = Keyframe.ofFloat(key, getY(startPoint, endPoint, midPoint, i * getDx(startPoint, endPoint) / count + startPoint[0]))
                key += keyStep
            }
            val pvhY = PropertyValuesHolder.ofKeyframe("translationY", *keyframes)
            val yxBouncer = ObjectAnimator.ofPropertyValuesHolder(view, pvhY, pvhX).setDuration(duration.toLong())
            //开始动画
            yxBouncer.start()
            return yxBouncer
        }

        private fun getDx(startPoint: FloatArray, endPoint: FloatArray): Float {
            return endPoint[0] - startPoint[0]
        }

        private fun getDy(startPoint: FloatArray, endPoint: FloatArray): Float {
            return endPoint[1] - startPoint[1]
        }

        /**
         * 这里是根据三个坐标点{（0,0），（300,0），（150,300）}计算出来的抛物线方程
         * y = ax² + bx + c
         *
         * @param x
         * @return
         */
        private fun getY(startPoint: FloatArray, endPoint: FloatArray, midPoint: FloatArray, x: Float): Float {
            val x1 = startPoint[0]
            val y1 = startPoint[1]
            val x2 = endPoint[0]
            val y2 = endPoint[1]
            val x3 = midPoint[0]
            val y3 = midPoint[1]
            val a: Float = (y1 * (x2 - x3) + y2 * (x3 - x1) + y3 * (x1 - x2)) / (x1 * x1 * (x2 - x3) + x2 * x2 * (x3 - x1) + x3 * x3 * (x1 - x2))
            val b = (y1 - y2) / (x1 - x2) - a * (x1 + x2)
            val c = y1 - x1 * x1 * a - x1 * b
            return a * x * x + b * x + c
        }
    }
}