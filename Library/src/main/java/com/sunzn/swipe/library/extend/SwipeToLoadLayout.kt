package com.sunzn.swipe.library.extend

import com.sunzn.swipe.library.OnLoadMoreListener
import com.sunzn.swipe.library.OnRefreshListener
import com.sunzn.swipe.library.SwipeToLoadLayout

inline fun SwipeToLoadLayout.doOnPrepare(
    crossinline action: () -> Unit,
): OnRefreshListener = setOnRefreshListener(onPrepare = action)

inline fun SwipeToLoadLayout.doOnRefresh(
    crossinline action: () -> Unit,
): OnRefreshListener = setOnRefreshListener(onRefresh = action)

inline fun SwipeToLoadLayout.doOnReset(
    crossinline action: () -> Unit,
): OnRefreshListener = setOnRefreshListener(onReset = action)

inline fun SwipeToLoadLayout.doOnCancel(
    crossinline action: () -> Unit,
): OnRefreshListener = setOnRefreshListener(onCancel = action)

inline fun SwipeToLoadLayout.setOnRefreshListener(
    crossinline onPrepare: () -> Unit = { },
    crossinline onRefresh: () -> Unit = { },
    crossinline onReset: () -> Unit = { },
    crossinline onCancel: () -> Unit = { },
): OnRefreshListener {
    val listener = object : OnRefreshListener {
        override fun onPrepare() {
            onPrepare.invoke()
        }

        override fun onRefresh() {
            onRefresh.invoke()
        }

        override fun onReset() {
            onReset.invoke()
        }

        override fun onCancel() {
            onCancel.invoke()
        }
    }
    setOnRefreshListener(listener)
    return listener
}

inline fun SwipeToLoadLayout.doOnLoadMore(
    crossinline action: () -> Unit,
): OnLoadMoreListener = setOnLoadMoreListener(onLoadMore = action)

inline fun SwipeToLoadLayout.setOnLoadMoreListener(
    crossinline onLoadMore: () -> Unit = { },
): OnLoadMoreListener {
    val listener = object : OnLoadMoreListener {
        override fun onLoadMore() {
            onLoadMore.invoke()
        }
    }
    setOnLoadMoreListener(listener)
    return listener
}