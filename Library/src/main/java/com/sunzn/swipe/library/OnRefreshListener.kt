package com.sunzn.swipe.library

/**
 * Created by sunzn on 2016/5/30.
 */
interface OnRefreshListener {
    fun onPrepare()
    fun onRefresh()
    fun onReset()
    fun onCancel()
}