package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout
import com.sunzn.swipe.library.view.SwipeProcessCircleView.Companion.LINE_COUNT

class CircleRefreshHeaderView : SwipeRefreshHeaderLayout {

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    private var headerHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_70)

    private lateinit var icon: SwipeProcessCircleView
    private lateinit var text: AppCompatTextView

    override fun onFinishInflate() {
        super.onFinishInflate()
        icon = findViewById(R.id.swipe_circle_icon)
        text = findViewById(R.id.swipe_circle_text)
    }

    override fun onRefresh() {
        icon.start()
        text.text = "正在刷新"
    }

    override fun onPrepare() {
        Log.d("CircleRefreshHeaderView", "onPrepare()")
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            if (y > headerHeight) {
                text.text = "释放刷新"
                icon.setAnimateValue(LINE_COUNT)
            } else {
                text.text = "下拉刷新"
                icon.setAnimateValue(LINE_COUNT * y / headerHeight)
            }
        }
    }

    override fun onRelease() {
        Log.d("CircleRefreshHeaderView", "onRelease()")
    }

    override fun onComplete() {
        icon.stop()
        text.text = "刷新完成"
    }

    override fun onReset() {
        icon.stop()
        text.text = "下拉刷新"
        icon.setAnimateValue(0)
    }

}