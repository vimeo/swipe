package com.sunzn.swipe.library.view

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class TMallRefreshHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context, attrs, defStyleAttr) {

    private lateinit var icon: AppCompatImageView
    private lateinit var text: AppCompatTextView
    private lateinit var mAnimDrawable: AnimationDrawable
    private val mHeaderHeight: Int

    init {
        mHeaderHeight = context.resources.getDimensionPixelOffset(R.dimen.refresh_header_height_100)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        icon = findViewById(R.id.swipe_tmall_icon)
        text = findViewById(R.id.swipe_tmall_text)
        mAnimDrawable = icon.background as AnimationDrawable
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onRefresh() {
        text.text = "正在刷新..."
        icon.setBackgroundResource(R.drawable.anim_header_wait_tm)
        mAnimDrawable = icon.background as AnimationDrawable
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onPrepare() {
        mAnimDrawable = icon.background as AnimationDrawable
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            if (y > mHeaderHeight) {
                text.text = "释放刷新"
            } else if (y < mHeaderHeight) {
                text.text = "下拉刷新"
            }
        }
    }

    override fun onRelease() {}
    override fun onComplete() {
        mAnimDrawable.stop()
        icon.setBackgroundResource(R.drawable.anim_header_pull_tm)
        text.text = "刷新完成"
    }

    override fun onReset() {
        mAnimDrawable.stop()
        icon.setBackgroundResource(R.drawable.anim_header_pull_tm)
        mAnimDrawable = icon.background as AnimationDrawable
        text.text = "下拉刷新"
    }

    override fun onCancel() {}
}