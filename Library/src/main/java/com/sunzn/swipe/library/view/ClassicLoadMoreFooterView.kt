package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeLoadMoreFooterLayout

/**
 * Created by sunzn on 2016/5/30.
 */
class ClassicLoadMoreFooterView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeLoadMoreFooterLayout(context, attrs, defStyleAttr) {

    private lateinit var text: AppCompatTextView
    private lateinit var success: AppCompatImageView
    private lateinit var progressBar: ProgressBar

    private val mFooterHeight: Int = resources.getDimensionPixelOffset(R.dimen.refresh_header_height_60)

    override fun onFinishInflate() {
        super.onFinishInflate()
        text = findViewById(R.id.footer_classic_text)
        success = findViewById(R.id.footer_classic_success)
        progressBar = findViewById(R.id.progressbar)
    }

    override fun onPrepare() {
        success.visibility = GONE
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            success.visibility = GONE
            progressBar.visibility = GONE
            if (-y >= mFooterHeight) {
                text.text = "释放加载更多"
            } else {
                text.text = "上拉加载更多"
            }
        }
    }

    override fun onLoadMore() {
        text.text = "正在加载"
        progressBar.visibility = VISIBLE
    }

    override fun onRelease() {}
    override fun onComplete() {
        text.text = "加载完毕"
        progressBar.visibility = GONE
        success.visibility = VISIBLE
    }

    override fun onReset() {
        success.visibility = GONE
    }

}