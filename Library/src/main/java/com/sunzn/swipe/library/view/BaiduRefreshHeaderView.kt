package com.sunzn.swipe.library.view

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.appcompat.widget.AppCompatImageView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class BaiduRefreshHeaderView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context!!, attrs, defStyleAttr) {

    private val rotateAnimation: Animation
    private val pullBackAnimRightCloudLinear: Animation
    private val pullBackAnimLeftCloudLinear: Animation
    private val pullBackAnimLeftLinear: Animation
    private val pullBackAnimRightLinear: Animation

    private lateinit var ivSun: AppCompatImageView
    private lateinit var ivRefresh: AppCompatImageView
    private lateinit var ivBackCloud: AppCompatImageView
    private lateinit var ivBackCloud2: AppCompatImageView
    private lateinit var ivBackCastle: AppCompatImageView
    private lateinit var ivBackCastle2: AppCompatImageView
    private lateinit var mAnimDrawable: AnimationDrawable

    init {
        rotateAnimation = AnimationUtils.loadAnimation(context, R.anim.rotate_sun)
        rotateAnimation.interpolator = LinearInterpolator()
        pullBackAnimLeftCloudLinear = AnimationUtils.loadAnimation(context, R.anim.pull_back_anim_left_cloud_linear)
        pullBackAnimRightCloudLinear = AnimationUtils.loadAnimation(context, R.anim.pull_back_anim_right_cloud_linear)
        pullBackAnimLeftLinear = AnimationUtils.loadAnimation(context, R.anim.pull_back_anim_left_linear)
        pullBackAnimRightLinear = AnimationUtils.loadAnimation(context, R.anim.pull_back_anim_right_linear)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        ivRefresh = findViewById(R.id.ivRefresh)
        ivSun = findViewById(R.id.iv_sun)
        ivBackCloud = findViewById(R.id.iv_back_cloud)
        ivBackCloud2 = findViewById(R.id.iv_back_cloud2)
        ivBackCastle = findViewById(R.id.iv_back_castle)
        ivBackCastle2 = findViewById(R.id.iv_back_castle2)
        mAnimDrawable = ivRefresh.background as AnimationDrawable

        //开启动画
        openAnimation()
    }

    private fun openAnimation() {
        //开启动画
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
        ivSun.clearAnimation()
        ivBackCloud.clearAnimation()
        ivBackCloud2.clearAnimation()
        ivBackCastle.clearAnimation()
        ivBackCastle2.clearAnimation()
        ivSun.startAnimation(rotateAnimation)
        ivBackCloud.startAnimation(pullBackAnimLeftCloudLinear)
        ivBackCloud2.startAnimation(pullBackAnimRightCloudLinear)
        ivBackCastle.startAnimation(pullBackAnimLeftLinear)
        ivBackCastle2.startAnimation(pullBackAnimRightLinear)
    }

    override fun onRefresh() {}
    override fun onPrepare() {
        Log.d("BaiduRefreshHeaderView", "onPrepare()")
        //开启动画
        openAnimation()
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            onMoveListener?.onMove(y)
        }
    }

    override fun onRelease() {
        Log.d("BaiduRefreshHeaderView", "onRelease()")
    }

    override fun onComplete() {}
    override fun onReset() {
        mAnimDrawable.stop()
        ivSun.clearAnimation()
        ivBackCloud.clearAnimation()
        ivBackCloud2.clearAnimation()
        ivBackCastle.clearAnimation()
        ivBackCastle2.clearAnimation()
    }

    private var onMoveListener: OnMoveListener? = null

    interface OnMoveListener {
        fun onMove(y: Int)
    }

    fun setOnMoveListener(onMoveListener: OnMoveListener?) {
        this.onMoveListener = onMoveListener
    }

}