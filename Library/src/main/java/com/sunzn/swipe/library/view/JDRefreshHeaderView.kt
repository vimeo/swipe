package com.sunzn.swipe.library.view

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshTrigger
import com.sunzn.swipe.library.SwipeTrigger

class JDRefreshHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr), SwipeTrigger, SwipeRefreshTrigger {

    private lateinit var main: LinearLayoutCompat
    private lateinit var wind: AppCompatImageView
    private lateinit var man: AppCompatImageView
    private lateinit var box: AppCompatImageView
    private lateinit var mAnimDrawable: AnimationDrawable
    private lateinit var mTwinkleAnim: Animation
    private val mHeaderHeight: Int

    init {
        mHeaderHeight = context.resources.getDimensionPixelOffset(R.dimen.refresh_header_height_80)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        man = findViewById(R.id.swipe_jd_man)
        main = findViewById(R.id.swipe_jd_main)
        wind = findViewById(R.id.swipe_jd_wind)
        box = findViewById(R.id.swipe_jd_box)
        mAnimDrawable = man.background as AnimationDrawable
        mTwinkleAnim = AnimationUtils.loadAnimation(context, R.anim.twinkle)
        wind.visibility = GONE
    }

    override fun onRefresh() {
        wind.visibility = VISIBLE
        box.visibility = GONE
        wind.startAnimation(mTwinkleAnim)
        main.alpha = 1.0f
        box.alpha = 1.0f
        if (!mAnimDrawable.isRunning) {
            mAnimDrawable.start()
        }
    }

    override fun onPrepare() {
        wind.clearAnimation()
        wind.visibility = GONE
        box.visibility = VISIBLE
        main.alpha = 0.3f
        box.alpha = 0.3f
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            val scale = y.toFloat() / mHeaderHeight.toFloat()
            if (y >= mHeaderHeight) {
                man.scaleX = 1f
                man.scaleY = 1f
                main.alpha = 1.0f
                box.alpha = 1.0f
                box.scaleX = 1f
                box.scaleY = 1f
            } else if (y > 0) {
                man.scaleX = scale
                man.scaleY = scale
                main.alpha = scale
                box.alpha = scale
                box.scaleX = scale
                box.scaleY = scale
            } else {
                man.scaleX = 0.4f
                man.scaleY = 0.4f
                box.scaleX = 0.5f
                box.scaleY = 0.5f
                main.alpha = 0.3f
                box.alpha = 0.3f
            }
        }
    }

    override fun onRelease() {
        mAnimDrawable.stop()
    }

    override fun onComplete() {}
    override fun onReset() {
        mAnimDrawable.stop()
        wind.clearAnimation()
        wind.visibility = GONE
        box.visibility = VISIBLE
        main.alpha = 1.0f
        box.alpha = 1.0f
    }

    override fun onCancel() {}
}