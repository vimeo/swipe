package com.sunzn.swipe.library.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import com.sunzn.swipe.library.R
import com.sunzn.swipe.library.SwipeRefreshHeaderLayout

class NoteRefreshHeaderView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwipeRefreshHeaderLayout(context, attrs, defStyleAttr) {

    private lateinit var icon: ImageView
    private lateinit var pen: ImageView
    private val mHeaderHeight: Int

    init {
        mHeaderHeight = context.resources.getDimensionPixelOffset(R.dimen.refresh_header_height_80)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        icon = findViewById(R.id.swipe_note_icon)
        pen = findViewById(R.id.swipe_note_pen)
    }

    override fun onRefresh() {
        pen.visibility = VISIBLE
        pen.alpha = 1.0f
    }

    override fun onPrepare() {
        pen.visibility = VISIBLE
        pen.alpha = 0.3f
    }

    override fun onMove(y: Int, isComplete: Boolean, automatic: Boolean) {
        if (!isComplete) {
            val scale = y.toFloat() / mHeaderHeight.toFloat()
            if (y >= mHeaderHeight) {
                icon.scaleX = 1f
                icon.scaleY = 1f
                pen.alpha = 1.0f
                pen.scaleX = 1f
                pen.scaleY = 1f
            } else if (y > 0) {
                icon.scaleX = scale
                icon.scaleY = scale
                pen.alpha = scale
                pen.scaleX = scale
                pen.scaleY = scale
            } else {
                icon.scaleX = 0.4f
                icon.scaleY = 0.4f
                pen.scaleX = 0.5f
                pen.scaleY = 0.5f
                pen.alpha = 0.3f
            }
        }
    }

    override fun onRelease() {}
    override fun onComplete() {}
    override fun onReset() {
        pen.visibility = VISIBLE
        pen.alpha = 1.0f
    }

    override fun onCancel() {}
}