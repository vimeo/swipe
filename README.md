# Swipe

[![](https://jitpack.io/v/com.sunzn/swipe.svg)](https://jitpack.io/#com.sunzn/swipe)
[![](https://img.shields.io/badge/License-Apache%202.0-orange.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

An Android library that help you to build app.

### Gradle

#### 1. Add the JitPack repository to your build file.

```groovy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
 }
```

#### 2. Add the dependency.

```groovy
dependencies {
    implementation 'com.sunzn.swipe:swipe:1.1.2'
}
```

### How to use

#### 1. Add SwipeToLoadLayout to your layout.

```xml
<com.sunzn.swipe.library.SwipeToLoadLayout
    android:id="@+id/swipeToLoadLayout"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <include
        android:id="@id/swipe_refresh_header"
        layout="@layout/header_swipe_note" />

    <TextView
        android:id="@id/swipe_target"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="#ef3939"
        android:gravity="center"
        android:text="下拉刷新"
        android:textColor="#FFFFFF"
        android:textSize="24sp" />

</com.sunzn.swipe.library.SwipeToLoadLayout>
```

#### 2. Implement OnRefreshListener interface.

```java
SwipeToLoadLayout swipeToLoadLayout = findViewById(R.id.swipeToLoadLayout);

swipeToLoadLayout.setOnRefreshListener(this);

@Override
public void onRefresh() {
    swipeToLoadLayout.postDelayed(new Runnable() {
        @Override
        public void run() {
            swipeToLoadLayout.setRefreshing(false);
        }
    }, 3000);
}
```

## License

    Copyright [2016-2024] sunzn

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
